
from celery_task_1.tasks import test_task
# 直接导入函数，调用函数，和celery没有任何关系
# test_task(666)

# 要将任务交给celery执行
# 1) 异步任务（立即执行）
# task = test_task.delay(666)
# print(task.id)


# 2） 延迟任务（达到设定的延迟时间后再去异步执行）
from datetime import datetime, timedelta
eta = datetime.utcnow() + timedelta(seconds=10)
t2 = test_task.apply_async(args=(888,), eta=eta)
print(t2)



# 3) 定时任务（在worker服务以外，在启动一个beat服务，定时帮我们自动添加任务) => 定时更新轮播图
