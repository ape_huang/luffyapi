# 1) 基础使用
"""
import redis
r = redis.Redis(host='127.0.0.1', port=6379, db=1, password='Redis123', decode_responses=True)
r.set('a', '100')
print(r.get('a'))

r.setex('b', '10', '100')
"""
# 2）连接池使用
import redis
poll = redis.ConnectionPool(password='Redis123', decode_responses=True)
r = redis.Redis(connection_pool=poll)
# r.rpush('arr', 1, 2, 3, 4)
r.zadd('game', {'bob': 10, 'tom': 5, 'jerry': 7, 'ben': 8})
print(r.zrange('game', 0, 2))
print(r.zrevrange('game', 0, 2))


# 3）可以将redis分装起来，直接拿redis对象使用redis数据库
from redis import Redis, ConnectionPool
pool = ConnectionPool(password='Redis123', decode_response=True, max_connections=100000)
redis = Redis(connection_pool=pool)
