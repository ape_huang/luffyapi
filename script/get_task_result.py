from celery_task_1.celery import app

from celery.result import AsyncResult

id = '4d1f270d-f6e8-455d-bfb5-e46ce5659438'
if __name__ == '__main__':
    task_result = AsyncResult(id=id, app=app)
    if task_result.successful():
        result = task_result.get()
        print(result)
    elif task_result.failed():
        print('任务失败')
    elif task_result.status == 'PENDING':
        print('任务等待被执行')
    elif task_result.status == 'RETRY':
        print('任务异常后正在重试')
    elif task_result.status == 'STARTED':
        print('任务已经开始被执行')
