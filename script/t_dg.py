import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "luffyapi.settings.dev")
django.setup()


# from .t_redis import redis

from django.core.cache import cache
# cache.set('name', 'django')
# cache.set('age', '18', 10)
# print(cache.get('name'))

from user import models, serializers

user = models.User.objects.first()
serializer_data  = serializers.LoginSerializer(user).data
print(serializer_data)


# 原生redis不能直接操作drf序列化后的结果
# from .t_redis import redis
# redis.set('d1', serializer_data)

# cache.set('user_data', serializer_data)
print(cache.get('user_data'))