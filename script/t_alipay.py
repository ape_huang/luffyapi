'''




from alipay import AliPay

# Making sure your key file is adhered to standards.
# you may find examples at tests/certs/ali/ali_private_key.pem
# app_private_key_string = open("/path/to/your/private/key.pem").read()
# alipay_public_key_string = open("/path/to/alipay/public/key.pem").read()


# 应用私钥
app_private_key_string = """-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1VFp6zt+L3bGKVX8Edtik2SwbemLgy6t6LaLrSMtLG0TKOGC140w6NUwYDGtNjsNr3DZ7oGfjCS0ShEAw44ljumVm0pDa2obn/6qOnJeUqwjVnTzCj72xvRwMMfUfeyBJWh0R0wUHrQWhIqRY6yAVpZriUW0EioxNa1v1jyYjLkyKeAD92gU9JMjEeScmw/K0SVOReKM55nf9z7lPrs7kJaebqqgc5QBnZpulVqCFudi/6yqeEzIPG3yawRuZShvHxVhhlzNOUzgS/5Y+hUB/NjlyWVQJ3fBtfBVL+kOY7QXbME4XqOlsHCBjo9CebfyKuCrg6XfgOJS6U1ck1qEPwIDAQABAoIBAGxI66+Rf7iwXl9VWqPl7eBMWJMy3rM02usq3zSAisnmhLdX4odxEMbgkbIDBZ5l08ifTMGZ41fMughvISgj3ETzphIyXI8GSgepytfofqEQ6M9WhplI5RSRuPHKPr+9csF+iKg4om4DlPqrLKqgQnWx3dzNGV+vM1PAOulR0U59NvunRS3Bhepda9JtNJAJ7uIBg8LASb7bqAPH1xwUW9+FJYCh1jjgNGWrbtKWGLYZ2HWVD59EtYnPmwogEuzAcjfcvsF4ijD7T7b5XAZpdjnIV6cNnwxIyfECF7E0123q8RgTRZ7bNqbaWmwBAriltrH4vTnI1qx6ogM/FXo/24ECgYEA71rdbPAXvxScAaa+evBCI+VdsN7Xv8P81IMQ3nQS7uTZrPq1WeUoTNhqeL0NvHBfboyP+m/otPF55bvQlx9bdfKWVVdPZ9tmMI1Gang1HEvVpp3dHVfV2QY9K8Jf53ejghHSEzkDROuVvjUPCPsy08aBv6p8UsxAK8kwGNRhOlECgYEA5CcGRvYGdxZznsHZZ2fmAfBc6qi6H0xi+I8dGxt1uk+ycHBcpQ/VVZ0kuMLepTzADDdq1GG+YO6iYf5/3DjQ87FKZKEhMLiBwKA0+BTue3GN/lIPJ3A+1zhVXmf9vjeM+W37/4w93o1hcq/f9hKR7atS5HEMHr+FA/W4qz/+oY8CgYBDcRTm9xeZzZLPnE4bkcrxFlW52nBsJir9Lhj4tTgIfA8hgFrNA5bBYFFPx+Uw7XDWU2DWLeYh5dB0/OCfVESPDVVyonFLUDSRBZ2Lrmc4m92NXvnIpNEqn2lpBRHExJ8brS3GINBdAQxmVcjwvObraQlyGfWB1u9V5xBYTq8+oQKBgQCKcmfrvbW0pEorB2T+y9yWRp6LwzGWdosEXAwEfAhXs68LbhO6Ej6Zwk8bUx06VgAQKbzI/jtBpO2i24YJQ+0ARBBpuiFi6EPMeStLc6A4BnNs5azOeN2ZSlT7gc1HlXwVjDlEHH0axN5icvlL4H7vI2yctUDuk7eK7JxQRvgkXwKBgBSk2XmOhW1TrvU2l988uv3OfuLdGIet/FddDAMasI2/6SULupNMe+r7yjEocy7MvwXUzEknnCFpT2+FVklTFnwtpeuGiU0swTvVrGFSgGOiZKQTWYXzxYAbhImsBl8Aure1e0u5XLIN949PnBHGSX2fhMpqNEfBxmr5jbh3Y9jb
-----END RSA PRIVATE KEY-----"""
# 支付宝公钥
alipay_public_key_string = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnLPPu5e4iWMqDOLBbZt3G7Gd2EhZFuuGcwbsYpsXTzG1ZEPZSmfQvNm1Xyif0QxZ/7ahBWKOVoQX3yZUB2R2mB286kJRZlCZe9ZFX7oRz/bQQnOblReoHidCExW2S05QaB4h7Sqd9xu71Yae3HIOJU8eERQjcj518yYOtH1723EQyYJ42pkkcZP5Bu8HBuHRoRgHZLLajaTvrLHH4z92dRhHpLxXKxgmKxgb+7ZimG0XhLoEinHRsB5BGgDE2ZGrJjALyGYNmNjzo9NeW+gLACOi/ts8B2sqiO4b2Az+1Xo5OZq7ZOZg6keVGuvOKnAzcwebGd4qXf25Et9tbd2HhwIDAQAB
-----END PUBLIC KEY-----"""

# 支付对象
alipay = AliPay(
    appid="2016101800716608",
    app_notify_url=None,
    app_private_key_string=app_private_key_string,
    alipay_public_key_string=alipay_public_key_string,
    sign_type="RSA2",
    debug=False
)


"""dc_alipay = DCAliPay(
    appid="appid",
    app_notify_url="http://example.com/app_notify_url",
    app_private_key_string=app_private_key_string,
    app_public_key_cert_string=app_public_key_cert_string,
    alipay_public_key_cert_string=alipay_public_key_cert_string,
    alipay_root_cert_string=alipay_root_cert_string
)


# Forget about what I mentioned below if you don't know what ISV is
# either app_auth_code or app_auth_token should not be None
isv_alipay = ISVAliPay(
    appid="",
    app_notify_url=None,  # the default notify path
    app_private_key_string="",
    # alipay public key, do not use your own public key!
    alipay_public_key_string=alipay_public_key_string,
    sign_type="RSA" # RSA or RSA2
    debug=False  # False by default,
    app_auth_code=None,
    app_auth_token=None
)
"""


# 测试网关 https://openapi.alipaydev.com/gateway.do
# 支付连接: https://openapi.alipay.com/gateway.do? + order_string
geteway = 'https://openapi.alipaydev.com/gateway.do'
import time
order_string = alipay.api_alipay_trade_page_pay    (
    out_trade_no='%s' % time.time(),
    total_amount=50000,
    subject='核潜艇',
    return_url="http://localhost:8080",  # 前台回调接口
    notify_url="https://example.com/notify" # 后台回调接口
)

pay_url = geteway + '?' + order_string
print(pay_url)

'''


# from libs import iPay
# import time
# order_string = iPay.alipay.api_alipay_trade_page_pay(
#     out_trade_no='%s' % time.time(), # 后台服务器在创建订单表订单时声称的订单号
#     total_amount=50000, # 前台提交，后台校验通过的商品总价
#     subject='核潜艇', # 前台提交给后台的商品名
#     return_url="http://localhost:8080",  # 在项目settings中配置前台get同步回调接口，必须是公网地址（本地也可以测试）
#     notify_url="https://example.com/notify" # 在项目settings中配置后台post八次异步回调接口，必须是公网地址
# )
#
# pay_url = iPay.gateway + '?' + order_string
#
# print(pay_url)
#
# import time
# code =