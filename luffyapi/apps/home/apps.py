from django.apps import AppConfig


class HomeConfig(AppConfig):
    name = 'luffyapi.apps.home'
