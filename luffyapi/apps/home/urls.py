from django.urls import path, include
from utils.router import router
from . import views
# 注册ViewSet的路由
router.register('banners', views.BannerViewSet, 'banner')

urlpatterns = [
    path('', include(router.urls)),
]