from django.db import models
from utils.model import BaseModel


# Create your models here.
class Banner(BaseModel):
    title = models.CharField(max_length=16, verbose_name='名称', unique=True)
    image = models.ImageField(upload_to='banner')
    link = models.CharField(max_length=64, verbose_name='跳转链接')
    info = models.TextField(verbose_name='详情')

    class Meta:
        db_table = 'luffy_banners'
        verbose_name_plural = '轮播图表'

    def __str__(self):
        return self.title
