from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework import mixins
from . import models, serializers
from django.conf import settings
from rest_framework.response import Response

from django.core.cache import cache


class BannerViewSet(ModelViewSet, mixins.ListModelMixin):
    queryset = models.Banner.objects.filter(is_delete=False,
                                            is_show=True).order_by('-orders')[:settings.BANNER_COUNT]
    serializer_class = serializers.BannerSerializer

    # 了解cors本质，处理还是在中间件中用插件，因为可以控制所有接口
    """
    def list(self, request, *args, **kwargs):
        print('111')
        response = super().list(request, *args, **kwargs)
        # 响应头设置'Access-Control-Allow-Origin'为'*'就代表该响应就可以对任何请求完成响应
        return Response(response.data, headers={'Access-Control-Allow-Origin': '*'})
    """

    # 要建立接口缓存，接口的请求逻辑必须自己实现
    # 有缓存走缓存，没缓存走数据库，同步给缓存
    def list(self, request, *args, **kwargs):
        banner_list = cache.get('banner_list')
        if not banner_list:
            print('走了数据库')
            response = super().list(request, *args, **kwargs)
            banner_list = response.data
            # 建立了一天的缓存，如果一一天内想要主动更新缓存，如何操作
            cache.set('banner_list', banner_list, 3600 * 24)
        return Response(banner_list)
