from django.urls import path, include
from utils.router import router
from . import views
# 注册ViewSet的路由
# router.register('pay', views.PayViewSet, 'pay')  #分类
# 支付接口（生成订单）
router.register('pay', views.PayViewSet, 'pay')
'''
1 支付接口（需要登录认证）：前台提交商品的信息，得到支付连接
    post方法

分析 支付宝回调：
        同步： get给前台 =》 前台可以在收到支付宝同步get回调时，ajax异步再给消息同步给后台，也采用get，后台处理前台的get请求
        异步： post给后台 =》 后台直接处理支付宝的post请求
    
2 支付回调接口（不需要登录认证）： 
    get方法处理前台来的同步回调（不一定能收到， 所以不能在该方法完成后台订单状态等信息操作）
    post方法处理支付宝来的异步回调
    
3 订单确认接口：随你前台任何时候来校验订单状态
'''
urlpatterns = [
    path('', include(router.urls)),
    path('success/', views.SuccessViewSet.as_view({'get': 'get', 'post': 'post'}))
]