# django-filter插件过滤类
from django_filters.filterset import FilterSet
from . import models
class CourseFilterSet(FilterSet):
    class Meta:
        model = models.Course
        fields = ['course_category']












# 前台接口：/course/free/?count=2, 代表支队前台返回2条数据
class CountFilter:
    def filter_queryset(self, request, queryset, view):
        count = request.query_params.get('count', None)
        try:
            return queryset[:int(count)]
        except:
            return queryset