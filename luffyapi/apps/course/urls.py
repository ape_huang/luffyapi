from django.urls import path, include
from utils.router import router
from . import views
# 注册ViewSet的路由
router.register('categories', views.CourseCategoryViewSet, 'categories')  #分类
router.register('free', views.CourseViewSet, 'free')  # 课程
router.register('chapters', views.ChapterViewSet, 'chapters')  # 章节
router.register('search', views.SearchCourseViewSet, 'search')  # 章节


urlpatterns = [
    path('', include(router.urls)),
]