from rest_framework.pagination import PageNumberPagination as DrfPageNumberPagination


class PageNumberPagination(DrfPageNumberPagination):
    # 默认一页显示的条数
    page_size = 3
    # url中携带页码的key
    page_query_param = 'page'
    # url中用户携带自定义一页条数的key
    page_size_query_param = 'page_size'
    # 用户最大可自定义一页的条数
    max_page_size = 3


from rest_framework.pagination import LimitOffsetPagination as DrfLimitOffsetPagination


class LimitOffsetPagination(DrfLimitOffsetPagination):
    default_limit = 2
    limit_query_param = 'limit'
    offset_query_param = 'offset'
    max_limit = 3


from rest_framework.pagination import CursorPagination as DrfCursorPagination


class CursorPagination(DrfCursorPagination):
    page_size = 2
    cursor_query_param = 'cursor'
    # ordering = '-created'
    page_size_query_param = 'page_size'
    max_page_size = 3