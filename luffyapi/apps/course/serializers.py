from rest_framework import serializers
from . import models


class CourseCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CourseCategory
        fields = ('name',)


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Teacher
        fields = ('name', 'role_name', 'title', 'signature', 'image', 'brief')


class CourseSerializer(serializers.ModelSerializer):
    teacher = TeacherSerializer(many=False)

    class Meta:
        model = models.Course
        fields = ('id', 'name', 'course_img', 'brief',
                  'attachment_path', 'pub_sections',
                  'price', 'students',
                  'period', 'sections', 'course_type_name',
                  'level_name', 'status_name',
                  'teacher', 'section_list'
                  )
        # fields = ('id', 'name', 'price', 'brief')


class CourseSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CourseSection
        fields = ('name', 'orders', 'section_link', 'duration', 'free_trail')


class CourseChapterSerializer(serializers.ModelSerializer):
    coursesections = CourseSectionSerializer(many=True)

    class Meta:
        model = models.CourseChapter
        fields = ('name', 'chapter', 'summary', 'coursesections')
