# Generated by Django 2.0.7 on 2020-03-07 22:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='course',
            old_name='create_time',
            new_name='created_time',
        ),
        migrations.RenameField(
            model_name='coursecategory',
            old_name='create_time',
            new_name='created_time',
        ),
        migrations.RenameField(
            model_name='coursechapter',
            old_name='create_time',
            new_name='created_time',
        ),
        migrations.RenameField(
            model_name='coursesection',
            old_name='create_time',
            new_name='created_time',
        ),
        migrations.RenameField(
            model_name='teacher',
            old_name='create_time',
            new_name='created_time',
        ),
    ]
