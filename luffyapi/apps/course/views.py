from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from . import models, serializers


# 课程分类群查
class CourseCategoryViewSet(GenericViewSet, ListModelMixin):
    queryset = models.CourseCategory.objects.filter(is_delete=False, is_show=True).all()
    serializer_class = serializers.CourseCategorySerializer


# 课程群查
# 分页组件：基础分页（采用）， 偏移分页， 游标分页（了解）
from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination, CursorPagination
from . import pagination

from rest_framework.filters import SearchFilter, OrderingFilter
from .filers import CountFilter, CourseFilterSet

# django-filter 插件 分类功能
from django_filters.rest_framework import DjangoFilterBackend
class CourseViewSet(GenericViewSet, ListModelMixin, RetrieveModelMixin):
    queryset = models.Course.objects.filter(is_delete=False, is_show=True).all()
    serializer_class = serializers.CourseSerializer
    # # 方法一：直接使用drf分页类，在视图类中完成分页类的必要配置
    # pagination_class = PageNumberPagination
    # PageNumberPagination.page_size = 2

    # 方法二： 自定义分页类继承drf分页类，在自定义分页类中完成配置，视图类中用自定义分页类
    # 基础分页器
    # pagination_class = pagination.PageNumberPagination
    # 偏移分页器 没有固定页码，自定义从偏移量开始往后查询自定义条数
    # pagination_class = pagination.LimitOffsetPagination
    # 游标分页器
    # pagination_class = pagination.CursorPagination
    # 过滤组件，实际开发，有多个过滤条件，要把优先级高的放在前面
    filter_backends = [SearchFilter, OrderingFilter, DjangoFilterBackend, CountFilter]
    # 参与搜索的字段
    search_filters = ['name', 'id', 'brief']

    # 允许排序
    ordering_filters = ['price', 'id', 'students']

    # 分类字段
    # filter_fields = ['course_category']

    # 过滤类
    filter_class = CourseFilterSet


class ChapterViewSet(GenericViewSet, ListModelMixin):
    queryset = models.CourseChapter.objects.filter(is_show=True, is_delete=False).all()
    serializer_class = serializers.CourseChapterSerializer

    filter_backends = [DjangoFilterBackend]
    filter_fields = ['course']


class SearchCourseViewSet(GenericViewSet, ListModelMixin):
    queryset = models.Course.objects.filter(is_delete=False, is_show=True).all()
    serializer_class = serializers.CourseSerializer
    pagination_class = pagination.PageNumberPagination
    filter_backends = [SearchFilter]
    search_fields = ['name']