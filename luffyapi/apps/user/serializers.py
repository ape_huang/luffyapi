from rest_framework import serializers
from rest_framework import exceptions
from django.conf import settings
from . import models
import re
from django.core.cache import cache


class LoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField()

    class Meta:
        model = models.User
        fields = ('id', 'username', 'password', 'icon')
        extra_kwargs = {
            'id': {
                'read_only': True,
            },
            'icon': {
                'read_only': True,
            },
            'password': {
                'write_only': True,
            },
        }

    def validate(self, attrs):
        # 多方式得到uesrname
        user = self._get_user(attrs)
        # user签发token
        token = self._get_token(user)
        # token用context属性携带给视图类
        self.context['token'] = token
        self.context['user'] = user
        return attrs

    def _get_user(self, attrs):

        username = attrs.get('username')
        # 手机号登录
        if re.match(r'^1[3-9]\d{9}$', username):
            user = models.User.objects.filter(mobile=username).first()
        # elif re.match(r'^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zrA-Z]{2,4})$', username):  # email不是唯一约束， 该功能不能提供
        #     user = models.User.objects.filter(email=username).first()
        else:
            user = models.User.objects.filter(username=username).first()
        if not user:
            raise exceptions.ValidationError({'user': 'not exist'})
        password = attrs.get('password')
        if not user.check_password(password):
            raise exceptions.ValidationError({'password': 'password error'})
        return user

    def _get_token(self, user):
        from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return token


class MobileLoginSerializer(serializers.ModelSerializer):
    # 覆盖
    mobile = serializers.CharField(required=True, write_only=True)
    # 自定义
    code = serializers.CharField(min_length=4, max_length=4, required=True, write_only=True)

    class Meta:
        model = models.User

        fields = ('id', 'username', 'icon', 'mobile', 'code')
        extra_kwargs = {
            'id': {
                'read_only': True,
            },
            'icon': {
                'read_only': True,
            },
            'username': {
                'read_only': True,
            },
        }

    # 手机号格式校验
    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise exceptions.ValidationError('mobile field error')
        return value

    def validate(self, attrs):
        mobile = self._check_code(attrs)
        user = self._get_user(mobile)
        # user签发token
        token = self._get_token(user)
        # token用context属性携带给视图类
        self.context['token'] = token
        self.context['user'] = user
        return attrs

    # 验证码校验，需要手机号与验证码开两者参与
    def _check_code(self, attrs):
        mobile = attrs.get('mobile')
        code = attrs.pop('code')
        old_code = cache.get(settings.SMS_CACHE_KEY % {'mobile': mobile})
        if code != old_code:
            raise exceptions.ValidationError({'code': 'double code error'})
        else:
            # 验证码的时效性: 一旦验证码验证通过，代该验证码已经使用，需要立即失效
            # cache.set(settings.SMS_CACHE_KEY % {'mobile': mobile}, '', -1)
            pass
        return mobile

    def _get_user(self, mobile):
        try:
            return models.User.objects.filter(mobile=mobile).first()
        except:
            raise exceptions.ValidationError({'mobile': 'user not exist'})

    def _get_token(self, user):
        from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return token


class RegisterSerializer(serializers.ModelSerializer):
    code = serializers.CharField(min_length=4, max_length=4, required=True, write_only=True)

    class Meta:
        model = models.User
        fields = ('mobile', 'password', 'code')
        extra_kwargs = {
            'password': {
                'min_length': 8,
                'max_length': 16,
                'write_only': True,
            }
        }

    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise exceptions.ValidationError('mobile field error')
        return value

    def validate(self, attrs):
        mobile = attrs.get('mobile')
        code = attrs.pop('code')
        old_code = cache.get(settings.SMS_CACHE_KEY % {'mobile': mobile})
        if code != old_code:
            raise exceptions.ValidationError({'code': 'double code error'})
        else:
            # 验证码的时效性: 一旦验证码验证通过，代该验证码已经使用，需要立即失效
            # cache.set(settings.SMS_CACHE_KEY % {'mobile': mobile}, '', -1)
            pass
        # 数据入库必须需要唯一账号：1）前台注册必须提供账号， 2）默认用手机号作为账号名，后期可以修改
        attrs['username'] = mobile
        return attrs

    def create(self, validated_data):  # 入库的数据：mobile, password, username
        return models.User.objects.create_user(**validated_data)


