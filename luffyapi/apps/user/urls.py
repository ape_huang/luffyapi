from django.urls import path, include
from utils.router import router
from . import views
# 注册ViewSet的路由
router.register('register', views.RegisterViewSet, 'register')

urlpatterns = [
    path('', include(router.urls)),
    path('login/', views.LoginViewSet.as_view({'post': 'login'})),
    path('mobile/', views.MobileViewSet.as_view({'post': 'check'})),
    path('mobile/', views.MobileViewSet.as_view({'post': 'check'})),
    path('sms/', views.SMSViewSet.as_view({'get': 'send'})),
    path('mobile/login/', views.MobileLoginViewSet.as_view({'post': 'login'})),
]