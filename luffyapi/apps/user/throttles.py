from rest_framework.throttling import SimpleRateThrottle
from django.core.cache import cache
from django.conf import settings
# 结合手机验证码接口来写
class SMSRateThrottle(SimpleRateThrottle):
    scope = 'sms'

    def get_cache_key(self, request, view):
        mobile = request.query_params.get('mobile', None)
        if not mobile:
            return None  # 不限制
        # 手机验证码发送失败不限制，只有发送成功才限制
        code = cache.get(settings.SMS_CACHE_KEY % {'mobile': mobile})
        if not code:
            return None

        return self.cache_format % {
            'scope': self.scope,
            'ident': mobile,
        }
