# 加载Django的环境
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "luffyapi.settings.dev")
# import django
# django.setup()

from celery import Celery

broker = 'redis://:Redis123@127.0.0.1:6379/1'
backend = 'redis://:Redis123@127.0.0.1:6379/2'

# worker配置
app = Celery(broker=broker, backend=backend, include=['celery_task.tasks'])

# 时区
app.conf.timezone = 'Asia/Shanghai'
# 是否使用utc
app.conf.enable_utc = False

# 定时任务的配置
from datetime import timedelta
from celery.schedules import crontab

app.conf.beat_schedule = {
    'test-task': {
        'task': 'celery_task.tasks.update_banner_list',
        'schedule': timedelta(seconds=10),
        # 'schedule': crontab(hour=8, day_of_week=1)  # 每周一早八点
        'args': (),
    }
}
