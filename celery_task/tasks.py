from .celery import app

from django.core.cache import cache
from home import models, serializers
from django.conf import settings

@app.task
def update_banner_list():
    queryset = models.Banner.objects.filter(is_delete=False,
                                            is_show=True).order_by('-orders')[:settings.BANNER_COUNT]
    banner_list = serializers.BannerSerializer(queryset, many=True).data
    # 拿不到request对象，所以头像的连接base_url要自己组装
    for banner in banner_list:
        banner['image'] = 'http://127.0.0.1:8000%s' % banner['image']
    cache.set('banner_list', banner_list, 86400)
    return True
